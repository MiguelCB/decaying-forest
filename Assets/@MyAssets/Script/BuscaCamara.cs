using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuscaCamara : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        Camera cam = GameObject.Find("Main Camera").GetComponent<Camera>();
        GetComponent<Canvas>().worldCamera = cam;
    }
}