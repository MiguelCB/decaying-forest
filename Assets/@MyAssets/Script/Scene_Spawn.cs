using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scene_Spawn : MonoBehaviour
{
    private GameObject player;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
        player.transform.Translate(transform.position);
    }

    // Update is called once per frame
    void Update()
    {
    }
}