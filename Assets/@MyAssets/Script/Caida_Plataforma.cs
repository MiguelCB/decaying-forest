using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class Caida_Plataforma : MonoBehaviour
{
    public ParticleSystem particle;
    public Transform posicionInicial;
    public Rigidbody2D rb;
    public float gravedad = 5;
    public float tiempoEncima = 3;
    public float Intervalo = 10;

    private Vector3 inicio;

    // Start is called before the first frame update
    void Start()
    {
        inicio = posicionInicial.position;
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag.Equals("Player"))
        {
            StartCoroutine(Caida());
        }
    }

    IEnumerator Caida()
    {
        particle.Play();
        yield return new WaitForSeconds(tiempoEncima);
        rb.gravityScale = gravedad;
        rb.constraints = RigidbodyConstraints2D.None;
        rb.constraints = RigidbodyConstraints2D.FreezeRotation;
        yield return new WaitForSeconds(Intervalo);
        rb.constraints = RigidbodyConstraints2D.FreezeAll;
        transform.position = inicio;
    }

    // Update is called once per frame
    void Update()
    {
    }
}