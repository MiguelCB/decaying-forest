using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using Random = System.Random;

public class Ataque_jefe : MonoBehaviour
{
    public Canvas fin;
    public Animator anim;
    public GameObject vida;
    public AudioClip end;
    private GameObject[] spawns;
    private bool stop = true;
    private GameObject bala;
    private GameObject raiz;

    // Start is called before the first frame update
    void Start()
    {
        spawns = GameObject.FindGameObjectsWithTag("Ataque_boss");
        raiz = GameObject.Find("RaizManager");
        for (int i = 0; i <= raiz.GetComponent<RaizManager>().raicesMuertas; i++)
        {
            indicadorvida();
        }
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag.Equals("Espada"))
        {
            indicadorvida();
        }
    }

    IEnumerator ataque()
    {
        int spawn = 0;
        spawn = new Random().Next(0, spawns.Length);
        bala = spawns[spawn];
        anim.SetBool("ataque", true);
        bala.GetComponent<Ataque_Inicio>().indicador.SetActive(true);
        yield return new WaitForSecondsRealtime(1);
        bala.GetComponent<Ataque_Inicio>().indicador.SetActive(false);
        bala.SetActive(true);
        yield return new WaitForSeconds(1);
        anim.SetBool("ataque", false);
        bala.SetActive(false);
        yield return new WaitForSeconds(4);
        stop = true;
    }

    private void indicadorvida()
    {
        vida.transform.localScale -= new Vector3(12.5f, 0, 0);
        if (vida.transform.localScale.x == 0)
        {
            AudioSource.PlayClipAtPoint(end, transform.position, 100);
            if (bala != null)
            {
                bala.SetActive(false);
            }

            Destroy(gameObject);
            fin.gameObject.SetActive(true);
            Time.timeScale = 0;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (stop)
        {
            stop = false;
            StartCoroutine(ataque());
        }
    }
}