using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Movimiento : MonoBehaviour
{
    public Canvas menu;
    public AudioClip ataque, muerte;
    public ContactFilter2D toque;
    public GameObject espada, rm;
    private Transform spawn;
    public Animator playerAnimator;
    public float tamañoRay = 3f;
    public float velSal = 5f;
    public float velMov = 5f;
    public Rigidbody2D rb;
    public LayerMask suelo;

    private static bool izFace = true,
        drFace = false,
        block = true;

    // Start is called before the first frame update
    void Start()
    {
        spawn = GameObject.Find("Spawn player").transform;
        rm = GameObject.Find("RaizManager");
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.IsTouching(toque))
        {
            AudioSource.PlayClipAtPoint(muerte, transform.position);
            DontDestroyOnLoad(rm);
            reinicio_spawm(col.gameObject);
        }

        if (col.gameObject.tag.Equals("Bala"))
        {
            AudioSource.PlayClipAtPoint(muerte, transform.position);
            transform.position = spawn.position;
        }
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag.Equals("Enemy"))
        {
            AudioSource.PlayClipAtPoint(muerte, transform.position);
            transform.position = spawn.position;
        }

        if (col.gameObject.tag.Equals("Suelo"))
        {
            StartCoroutine(wait(0.5f));
            playerAnimator.SetBool("Salto", false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        Debug.DrawRay(transform.position, Vector3.down * tamañoRay, Color.red);

        if (Input.GetButtonDown("Fire2") && block)
        {
            block = false;
            playerAnimator.SetBool("Espada", true);
            AudioSource.PlayClipAtPoint(ataque, transform.position);
            espada.SetActive(true);
            StartCoroutine(Espada());
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            menu.gameObject.SetActive(true);
            Time.timeScale = 0;
        }

        if (Input.GetButtonDown("Jump"))
        {
            RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down, tamañoRay, suelo);
            if (hit.collider != null && (hit.collider.gameObject.tag.Equals("Suelo") ||
                                         hit.collider.gameObject.tag.Equals("Plataforma")))
            {
                playerAnimator.SetBool("Salto", true);
                rb.velocity = new Vector2(rb.velocity.x, 0);
                rb.AddForce(new Vector2(0, velSal), ForceMode2D.Impulse);
            }
        }
    }

    void FixedUpdate()
    {
        int movanim = 0;
        float x = Input.GetAxisRaw("Horizontal");
        rb.velocity = new Vector2(x * velMov, rb.velocity.y);
        if (x < 0)
        {
            if (izFace)
            {
                transform.Rotate(0, 180, 0);
                izFace = false;
                drFace = true;
            }

            movanim = -1;
        }
        else if (x > 0)
        {
            if (drFace)
            {
                transform.Rotate(0, 180, 0);
                drFace = false;
                izFace = true;
            }

            movanim = 1;
        }

        playerAnimator.SetInteger("Movimiento", movanim);
    }

    private void reinicio_spawm(GameObject col)
    {
        if (col.tag.Equals("Boss") || col.tag.Equals("Ataque_boss"))
        {
            SceneManager.LoadScene("Jefe");
        }
    }

    IEnumerator Espada()
    {
        yield return new WaitForSecondsRealtime(0.5f);
        espada.SetActive(false);
        playerAnimator.SetBool("Espada", false);
        block = true;
    }

    IEnumerator wait(float Seconds)
    {
        yield return new WaitForSeconds(Seconds);
    }
}