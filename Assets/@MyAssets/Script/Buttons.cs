using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Buttons : MonoBehaviour
{
    public Canvas buttons, tuto, menu;
    public GameObject tuto2;

    // Start is called before the first frame update
    void Start()
    {
    }

    public void exit()
    {
        Application.Quit(0);
    }

    public void newGame()
    {
        SceneManager.LoadScene("SampleScene");
    }

    public void tutorial()
    {
        StartCoroutine(tutorialRoutine());
    }

    public void returnMenu()
    {
        SceneManager.LoadScene("Inicio");
    }

    public void resume()
    {
        menu.gameObject.SetActive(false);
        Time.timeScale = 1;
    }

    IEnumerator tutorialRoutine()
    {
        tuto.gameObject.SetActive(true);
        buttons.gameObject.SetActive(false);
        yield return new WaitForSecondsRealtime(5);
        Debug.Log("aaaaaaaaaaaaa");
        tuto2.SetActive(true);
        yield return new WaitForSecondsRealtime(5);
        tuto2.SetActive(false);
        tuto.gameObject.SetActive(false);
        buttons.gameObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
    }
}