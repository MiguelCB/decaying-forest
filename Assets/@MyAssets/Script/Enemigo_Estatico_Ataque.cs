using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using Random = System.Random;

public class Enemigo_Estatico_Ataque : MonoBehaviour
{
    public AudioClip shoot;
    public Transform spawn;
    public GameObject proyectil;
    private bool stop = true;

    // Start is called before the first frame update
    void Start()
    {
    }

    private void OnTriggerStay2D(Collider2D col)
    {
        if (col.gameObject.tag.Equals("Player") && stop)
        {
            GameObject bala;
            Vector3 direction = col.gameObject.transform.position - spawn.position;
            bala = Instantiate(proyectil, spawn.position, Quaternion.identity);
            AudioSource.PlayClipAtPoint(shoot, transform.position);
            bala.transform.LookAt(col.transform.position);
            bala.GetComponent<Rigidbody2D>().AddForce(bala.transform.forward * 3, ForceMode2D.Impulse);
            bala.transform.rotation = quaternion.identity;
            stop = false;
            StartCoroutine(wait());
        }
    }

    IEnumerator wait()
    {
        yield return new WaitForSeconds(1.5f);
        stop = true;
    }

    // Update is called once per frame
    void Update()
    {
    }
}