using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Puerta : MonoBehaviour
{
    public GameObject raiz;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag.Equals("Player"))
        {
            DontDestroyOnLoad(raiz);
            SceneManager.LoadScene("Jefe");
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
