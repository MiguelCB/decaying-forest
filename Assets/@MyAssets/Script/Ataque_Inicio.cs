using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ataque_Inicio : MonoBehaviour
{
    public GameObject indicador;
    // Start is called before the first frame update
    void Start()
    {
        indicador.SetActive(false);
        StartCoroutine(wait());
    }

    IEnumerator wait()
    {
        yield return new WaitForSecondsRealtime(0.2f);
        gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
    }
}